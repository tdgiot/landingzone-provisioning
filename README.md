# Oblcc Crave Interactive Landing Zone
This is the Oblcc landing zone for Crave Interactive.
The code can be found in oblcc github as well as in the Crave Interactive - Bitbucket repositoy.

Inside this AWS Crave Build account, a CodePipeline will deploy the cloudformation templates.

# Documentation
Documentation is provided in the `docs` folder. With mkdocs you can run the documentation locally.
Install [mkdocs](https://www.mkdocs.org) and start on the commandline.

$ mkdocs serve

# Set up local environment
Make sure a virtual environment is installed, at oblcc we use a python3 environment. Inside the python3 environment install the requirements.txt file

$ pip install -r requirements.txt

Also install/setup the pre-commit hook, so your CloudFormation templates are checked before committed

$ pre-commit install
