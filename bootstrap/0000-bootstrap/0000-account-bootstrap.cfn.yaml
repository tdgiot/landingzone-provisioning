---
AWSTemplateFormatVersion: 2010-09-09
Description: Crave Account Bootstrap
Parameters:
  BuildAccountId:
    Type: String
    Description: AWS account ID of the Crave build account?
    MaxLength: 12
    MinLength: 12
    Default: '514368322204' # crave-tdg-build
    ConstraintDescription: Must be a valid AWS account ID.
  LoggingAccountId:
    Type: String
    Description: What is AWS account ID of the Crave Logging account?
    MaxLength: 12
    MinLength: 12
    Default: '120753239793' # crave-tdg-logging
    ConstraintDescription: Must be a valid AWS account ID.
  CreateVpc:
    Type: String
    Default: 'true'
    AllowedValues:
      - 'true'
      - 'false'
    Description: Should a VPC be created in this account?
  VpcCidr:
    Type: String
    Description: What is the IPv4 CIDR block that should be used to create the VPC (if required)?
    MaxLength: 19
    MinLength: 0
    Default: 10.0.0.0/16
    AllowedPattern: "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))$"
    ConstraintDescription: Must be a valid IP range in x.x.x.x/x notation.
  EnableNatGateways:
    Type: String
    Default: 'false'
    Description: Deploy NAT gateways to all availability zones?
    AllowedValues:
      - 'true'
      - 'false'
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: Authentication parameters
        Parameters:
          - BuildAccountId
          - LoggingAccountId
      - Label:
          default: VPC parameters
        Parameters:
          - CreateVpc
          - VpcCidr
          - EnableNatGateways
Conditions:
  IsBuildAccount: !Equals [!Ref BuildAccountId, !Ref 'AWS::AccountId']
  CreateVpc: !Equals [!Ref CreateVpc, 'true']
Resources:
#
# CloudFormation Roles
#
  StackSetAdministratorRole:
    Type: AWS::IAM::Role
    Condition: IsBuildAccount
    Properties:
      RoleName: crave-cloudformation-stackset-administrator
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: cloudformation.amazonaws.com
            Action:
              - sts:AssumeRole
      Policies:
        - PolicyName: AssumeExecutionRole
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - sts:AssumeRole
                Resource:
                  - 'arn:aws:iam::*:role/crave-cloudformation-stackset-execution'
  StackSetExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: crave-cloudformation-stackset-execution
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              AWS:
                - !Ref BuildAccountId
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AdministratorAccess
  StackExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: crave-cloudformation-stack-execution
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              AWS:
                - !Ref BuildAccountId
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AdministratorAccess

#
# SSM Parameters
#
  BuildAccountIdParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /crave/landing-zone/accounts-ids/build
      Type: String
      Value: !Ref BuildAccountId
      Description: Build Account Id
  LoggingAccountIdParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /crave/landing-zone/accounts-ids/logging
      Type: String
      Value: !Ref LoggingAccountId
      Description: Logging Account Id
  VpcCidrParameter:
    Type: AWS::SSM::Parameter
    Condition: CreateVpc
    Properties:
      Name: /crave/landing-zone/vpc/cidr
      Type: String
      Value: !Ref VpcCidr
      Description: VPC IPv4 CIDR Block
  VpcEnableNatGatewaysParameter:
    Type: AWS::SSM::Parameter
    Condition: CreateVpc
    Properties:
      Name: /crave/landing-zone/vpc/enable-nat-gateways
      Type: String
      Description: Enable NAT Gateways
      Value: !Ref EnableNatGateways
