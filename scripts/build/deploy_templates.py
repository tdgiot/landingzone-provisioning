#!/usr/bin/env python
"""
Deploy CloudFormation StackSets
"""

from __future__ import print_function

import re
import sys
import os
import boto3
import botocore
import json
import yaml
import time
import functools
import pickle
from botocore.client import ClientError
from botocore.waiter import SingleWaiterConfig, Waiter

TEMPLATE_REGEX = r'^.*\.cfn\.(json|ya?ml)$'
TEMPLATE_DIR = sys.argv[1] if len(sys.argv) > 1 else '.'

STACKSET_ADMIN_ROLE_ARN = 'arn:aws:iam::514368322204:role/crave-cloudformation-stackset-administrator'
STACKSET_EXEC_ROLE_NAME = 'crave-cloudformation-stackset-execution'
STACK_EXEC_ROLE_NAME = 'crave-cloudformation-stack-execution'

TAGS = [
    {
        'Key': 'ApplicationName',
        'Value': 'Crave Landing Zone'
    }
]

client = boto3.client('cloudformation')

def profile(fn):
    @functools.wraps(fn)
    def with_profiling(*args, **kwargs):
        start_time = time.time()
        ret = fn(*args, **kwargs)
        elapsed_time = time.time() - start_time
        print('Operation took {:.1f}s to complete'.format(elapsed_time))
        return ret

    return with_profiling


class File:

    def __init__(self, root, basename, convert_yaml=False):
        self.basename = basename
        self.root = root
        self.path = os.path.join(self.root, self.basename)

        try:
            content = open(self.path).read()
            if convert_yaml:
                self.body = yaml.load(content)
            else:
                self.body = content
        except Exception as e:
            print(e)
            self.body = None

    def __str__(self):
        return self.basename


class Account:
    def __init__(self, id, alias, email):
        self.id = id
        self.alias = alias
        self.email = email


class AccountList:
    def __init__(self, filepath):
        with open(filepath, 'r') as inputfile:
            content = inputfile.read()
        self.accounts = yaml.load(content).get('Accounts', [])

    @staticmethod
    def exactly_one_result(items):
        if len(items) == 0 or len(items) > 1:
            raise ValueError
        return Account(items[0]['Id'], items[0]['Alias'], items[0]['Email'])


    def get_account_by_id(self, id):
        matches = [
            account for account in self.accounts
            if str(account['Id']).zfill(12) == str(id).zfill(12)
        ]
        return self.exactly_one_result(matches)

    def get_account_by_alias(self, alias):
        matches = [
            account for account in self.accounts
            if account['Alias'] == alias
        ]
        return self.exactly_one_result(matches)


class Template(File):

    @property
    def configuration(self):
        filename = self.basename.replace('.cfn.', '.settings.')
        return Configuration(self.root, filename, True)

    def deploy(self, accounts):
        # Skip broken files
        if self.body is None:
            print('Invalid YAML in {}'.format(self.path), file=sys.stderr)
            return
        elif self.configuration.body is None:
            print('Invalid YAML in {}'.format(self.configuration.path), file=sys.stderr)
            return
        elif self.configuration.type not in ['AWS::CloudFormation::Stack', 'AWS::CloudFormation::StackSet']:
            print('Invalid type in {}'.format(self.configuration.path), file=sys.stderr)
            return

        # Create object corresponding to type
        if self.configuration.type == 'AWS::CloudFormation::Stack':
            for instance in self.configuration.instances:
                instance = Instance(instance)
                stack = Stack(self, instance, accounts)
                stack.deploy()
        elif self.configuration.type == 'AWS::CloudFormation::StackSet':
            stack = StackSet(self, accounts)
            stack.deploy()

class Instance:

    def __init__(self, instance):
        self.account_alias = instance['AccountAlias']
        self.region = instance['Region']

class Configuration(File):

    @property
    def type(self):
        return self.body['Type']

    @property
    def stack_name(self):
        return self.body['StackName']

    @property
    def stack_set_name(self):
        return self.body['StackSetName']

    @property
    def region(self):
        return self.body['Region']

    @property
    def instances(self):
        return self.body['Instances']

    @property
    def capabilities(self):
        return self.body['Capabilities']


class Stack:

    def __init__(self, template, instance, accounts):
        self.template_body = template.body
        self.name = template.configuration.stack_name
        self.capabilities = template.configuration.capabilities
        self.account_alias = instance.account_alias
        self.account_id = accounts.get_account_by_alias(instance.account_alias).id
        self.region = instance.region

        self.client = self.get_client(
            account_id=self.account_id,
            region_name=self.region,
        )

        if not self.exists():
            self.exists = False
            self.template_changed = False
        else:
            self.exists = True
            self.template_changed = (self.template_body != self.get_deployed_template())
            self.tags_changed = (TAGS.sort() != self.tags.sort())

    def get_client(self, account_id, region_name):
        response = boto3.client('sts').assume_role(
            RoleArn='arn:aws:iam::{}:role/{}'.format(account_id, STACK_EXEC_ROLE_NAME),
            RoleSessionName='crave-landing-zone-provisioning'
        )
        credentials = response['Credentials']

        return boto3.client(
            'cloudformation',
            region_name=region_name,
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    def wait(self, operation_name):
        waiter = self.client.get_waiter(operation_name)
        try:
            waiter.wait(StackName=self.name)
        except botocore.exceptions.WaiterError:
            pass

    def exists(self):
        try:
            response = self.client.describe_stacks(StackName=self.name)
        except botocore.exceptions.ClientError:
            self.tags = []
            return False

        # Remove stacks that are in rollback status
        status = response['Stacks'][0]['StackStatus']
        if status == 'ROLLBACK_COMPLETE' or \
           status == 'ROLLBACK_FAILED' or \
           status == 'DELETE_FAILED' or \
           status == 'UPDATE_ROLLBACK_FAILED' or \
           status == 'REVIEW_IN_PROGRESS':
            self.delete()
            self.tags = []
            return False
        else:
            self.tags = response['Stacks'][0]['Tags']
            return True

    def get_deployed_template(self):
        try:
            response = self.client.get_template(
                StackName=self.name,
                TemplateStage='Original',
            )
            return response['TemplateBody']
        except ClientError as error:
            if error.response['Error']['Code'] == 'StackNotFoundException':
                return None
            else:
                raise error

    @profile
    def create(self):
        print('Creating stack {} in {}'.format(self.name, self.account_alias))
        self.client.create_stack(
            StackName=self.name,
            TemplateBody=self.template_body,
            Tags=TAGS,
            Capabilities=self.capabilities
        )
        self.wait('stack_create_complete')

    @profile
    def update(self):
        print('Updating stack {} in {}'.format(self.name, self.account_alias))
        try:
            self.client.update_stack(
                StackName=self.name,
                TemplateBody=self.template_body,
                Tags=TAGS,
                Capabilities=self.capabilities
            )
            waiter = self.client.get_waiter('stack_update_complete')
            waiter.wait(StackName=self.name)
        except ClientError as e:
            if (
                "ValidationError" == e.response.get("Error", {}).get("Code", "") and
                "No updates are to be performed." == e.response.get("Error", {}).get("Message", "")
            ):
                print('No updates are to be performed.')
                return None
            else:
                raise e

    @profile
    def delete(self):
        print('Deleting stack {} in {}'.format(self.name, self.account_alias))
        self.client.delete_stack(
            StackName=self.name
        )
        self.wait('stack_delete_complete')

    def deploy(self):
        if not self.exists:
            self.create()
        elif self.template_changed or self.tags_changed:
            self.update()
        else:
            print('No changes for {}'.format(self.name))


class StackSet:

    def __init__(self, template, accounts):
        self.template_body = template.body
        self.name = template.configuration.stack_set_name
        self.capabilities = template.configuration.capabilities

        self.required_instances = [
            {
                'Account': accounts.get_account_by_alias(instance['AccountAlias']).id,
                'Region': instance['Region'],
            }
            for instance in template.configuration.instances
        ]

        response = self.describe()
        if response is None:
            self.exists = False
            self.template_changed = False
            self.tags_changed = False
        else:
            self.exists = True
            self.template_changed = (self.template_body != response['StackSet']['TemplateBody'])
            self.tags_changed = (TAGS.sort() != response['StackSet']['Tags'].sort())

    def describe(self):
        try:
            return client.describe_stack_set(
                StackSetName=self.name,
            )
        except ClientError as error:
            if error.response['Error']['Code'] == 'StackSetNotFoundException':
                return None
            else:
                raise error

    def wait(self, operation_id):
        waiter = get_waiter()
        waiter.wait(
            StackSetName=self.name,
            OperationId=operation_id,
        )

    def create(self):
        print('Creating stack set {}'.format(self.name))
        client.create_stack_set(
            StackSetName=self.name,
            TemplateBody=self.template_body,
            Capabilities=self.capabilities,
            Tags=TAGS,
            AdministrationRoleARN=STACKSET_ADMIN_ROLE_ARN,
            ExecutionRoleName=STACKSET_EXEC_ROLE_NAME,
        )

    @profile
    def update(self):
        print('Updating stack set {}'.format(self.name))
        response = client.update_stack_set(
            StackSetName=self.name,
            TemplateBody=self.template_body,
            Capabilities=self.capabilities,
            Tags=TAGS,
            OperationPreferences={
                'MaxConcurrentPercentage': 100,
                'FailureTolerancePercentage': 100,
            },
            AdministrationRoleARN=STACKSET_ADMIN_ROLE_ARN,
            ExecutionRoleName=STACKSET_EXEC_ROLE_NAME,
        )

        self.wait(response['OperationId'])


    @profile
    def create_instances(self, instances):
        # Create instances per region
        for region, accounts in instances.items():
            print('Creating stack set instances:\n  Stack: {}\n  Region: {}\n  Accounts: {}'.format(self.name, region, json.dumps(accounts)))
            response = client.create_stack_instances(
                StackSetName=self.name,
                Accounts=accounts,
                Regions=[region],
                OperationPreferences={
                    'FailureTolerancePercentage': 100,
                    'MaxConcurrentPercentage': 100,
                },
            )

            self.wait(response['OperationId'])

    @profile
    def update_instances(self, instances):
        # Update instances per region
        for region, accounts in instances.items():
            print('Update stack set instances:\n  Stack: {}\n  Region: {}\n  Accounts: {}'.format(self.name, region, json.dumps(accounts)))
            response = client.update_stack_instances(
                StackSetName=self.name,
                Accounts=accounts,
                Regions=[region],
                OperationPreferences={
                    'FailureTolerancePercentage': 50,
                    'MaxConcurrentPercentage': 50,
                },
            )

            self.wait(response['OperationId'])

    @profile
    def delete_instance(self, instance):
        account = instance['Account']
        region = instance['Region']
        print('Deleting stack set instance {} {} {}'.format(self.name, account, region))
        response = client.delete_stack_instances(
            StackSetName=self.name,
            Accounts=[account],
            Regions=[region],
            RetainStacks=True,
        )

        self.wait(response['OperationId'])

    @property
    def deployed_instances(self):
        kwargs = {
            'StackSetName': self.name
        }
        instances = []
        while 'NextToken' not in kwargs or kwargs['NextToken']:
            response = client.list_stack_instances(**kwargs)
            instances += response['Summaries']
            kwargs['NextToken'] = response['NextToken'] if 'NextToken' in response else None

        return instances

    def deploy(self):

        if not self.exists:
            self.create()

        elif self.template_changed or self.tags_changed:
            self.update()

        else:
            print('No changes for {}'.format(self.name))

        required_instances = self.required_instances
        deployed_instances = self.deployed_instances

        update_instances = {}
        # Find out whether deployed StackSet instances are in the list of required instances
        for deployed_instance in deployed_instances:
            is_required = next(
                (
                    True
                    for required_instance in required_instances
                    if required_instance['Account'] == deployed_instance['Account']
                    if required_instance['Region'] == deployed_instance['Region']
                ),
                False
            )

            if not is_required:
                stack_set.delete_instance(deployed_instance)
                print('StackSet "{}" should not be targetted to account {}, alter script to remove it'.format(self.name, deployed_instance['Account']))
            elif not self.template_changed and deployed_instance['Status'] == 'OUTDATED':
            # Assemble update_instances like this: {eu-central-1: [123456789, 99999999]}
                update_instances.setdefault(
                    deployed_instance['Region'], []).append(
                        deployed_instance['Account'])

        # Find out whether required StackSet instances are already deployed
        create_instances = {}
        for required_instance in required_instances:
            is_deployed = next(
                (
                    True
                    for deployed_instance in deployed_instances
                    if required_instance['Account'] == deployed_instance['Account']
                    if required_instance['Region'] == deployed_instance['Region']
                ),
                False
            )

            # Assemble create_instances like this: {eu-central-1: [123456789, 99999999]}
            if not is_deployed:
                create_instances.setdefault(
                    required_instance['Region'], []).append(
                        required_instance['Account'])

        # Create instances when applicable
        if create_instances:
            self.create_instances(create_instances)

        # Update instances when applicable
        if update_instances:
            self.update_instances(update_instances)



def get_waiter():
    waiter_config = SingleWaiterConfig({
      'delay': 10,
      'operation': 'DescribeStackSetOperation',
      'maxAttempts': 360,
      'acceptors': [
        {
          'argument': 'StackSetOperation.Status',
          'expected': 'SUCCEEDED',
          'matcher': 'path',
          'state': 'success'
        },
        {
          'argument': 'StackSetOperation.Status',
          'expected': 'FAILED',
          'matcher': 'path',
          'state': 'failure'
        },
        {
          'argument': 'StackSetOperation.Status',
          'expected': 'STOPPED',
          'matcher': 'path',
          'state': 'failure'
        },
        {
          'expected': 'ValidationError',
          'matcher': 'error',
          'state': 'failure'
        }
      ]
    })

    return Waiter('StackSetOperationComplete', waiter_config, client.describe_stack_set_operation)


def main():
    pattern = re.compile(TEMPLATE_REGEX)
    accounts = AccountList('./data/accounts.yaml')

    # Note: Excluding '0000' folders, as they are not part of Stack operations
    templates = [
        Template(root, basename)
        for root, _, files in os.walk(TEMPLATE_DIR)
        for basename in files
        if pattern.match(basename) and not '/0000-' in root
    ]

    # Sort based on the path (those include numbered folders)
    templates.sort(key=lambda template: template.path)

    for template in templates:
        template.deploy(accounts)


if __name__ == '__main__':
    main()
