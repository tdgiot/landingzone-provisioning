#!/usr/bin/env python
"""
Retrieve CloudFormation StackSets status
"""

from __future__ import print_function

import boto3
from botocore.client import ClientError

client = boto3.client('cloudformation')


def get_stack_sets():
    kwargs = {
        'Status': 'ACTIVE'
    }
    while True:
        response = client.list_stack_sets(**kwargs)
        for stack_set in response['Summaries']:
            yield stack_set
        if 'NextToken' in response:
            kwargs['NextToken'] = response['NextToken']
        else:
            break

def get_stack_instances(stack_set_name):
    kwargs = {
        'StackSetName': stack_set_name
    }
    instances = []
    while 'NextToken' not in kwargs or kwargs['NextToken']:
        response = client.list_stack_instances(**kwargs)
        instances += response['Summaries']
        kwargs['NextToken'] = response['NextToken'] if 'NextToken' in response else None

    return instances

def main():
    stack_sets = get_stack_sets()
    for stack_set in stack_sets:
        name = stack_set['StackSetName']
        instances = get_stack_instances(name)
        print(name)
        for instance in instances:
            print('\t{} - {}'.format(instance['Account'], instance['Status']))
        print('')

if __name__ == '__main__':
    main()
