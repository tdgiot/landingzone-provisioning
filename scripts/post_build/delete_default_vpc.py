#!/usr/bin/env python
from __future__ import print_function

import os
import boto3
import yaml
import argparse
from botocore.exceptions import ClientError

STACK_EXEC_ROLE_NAME = 'crave-cloudformation-stack-execution'

def get_session(account_id):
    response = boto3.client('sts').assume_role(
        RoleArn='arn:aws:iam::{}:role/{}'.format(account_id, STACK_EXEC_ROLE_NAME),
        RoleSessionName='crave-landing-zone-provisioning'
    )
    credentials = response['Credentials']

    return boto3.Session(
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )


def get_regions():
    """
    Build a region list
    """
    regions = boto3.client('ec2').describe_regions()['Regions']
    return [
        region['RegionName'] for region in regions
    ]


class DefaultVpc():
    def __init__(self, client):
        self.client = client
        self.id = self.get_default_vpc_id()

    def delete(self):
        if self.id:
            self.delete_igw()
            self.delete_subnets()
            self.delete_vpc()

    def get_default_vpc_id(self):
        """
        Get Default VPC Id
        """
        vpcs = self.client.describe_vpcs()
        vpc_id = None
        if vpcs.get('Vpcs'):
            for vpc in vpcs.get('Vpcs'):
                default = True if vpc.get('IsDefault') is True else False
                if default:
                    vpc_id = vpc.get('VpcId')
        return vpc_id

    def delete_igw(self):
        """
        Delete Internet Gateway
        """
        igw = self.client.describe_internet_gateways(
            Filters=[{
                'Name': 'attachment.vpc-id',
                'Values': [self.id]
            }]
        )

        if igw.get('InternetGateways'):
            try:
                for igw in igw.get('InternetGateways'):
                    igw_id = igw.get('InternetGatewayId')
                    self.client.detach_internet_gateway(
                        InternetGatewayId=igw_id,
                        VpcId=self.id
                    )
                    self.client.delete_internet_gateway(
                        InternetGatewayId=igw_id
                    )
            except ClientError as e:
                print(e.message)

    def delete_subnets(self):
        """
        Delete Subnets
        """
        subnets = self.client.describe_subnets(
            Filters=[{
                'Name': 'vpc-id',
                'Values': [self.id]
            }]
        )
        if subnets.get('Subnets'):
            for subnet in subnets.get('Subnets'):
                try:
                    subnet_id = subnet.get('SubnetId')
                    self.client.delete_subnet(SubnetId=subnet_id)
                except ClientError as e:
                    print(e.message)

    def delete_vpc(self):
        try:
            self.client.delete_vpc(
                VpcId=self.id
            )
        except ClientError as e:
            print(e.message)


def main():
    print('Deleting default VPCs:')
    content = open('./data/accounts.yaml').read()
    body = yaml.safe_load(content)
    accounts = body.get('Accounts', {})
    regions = get_regions()

    parser = argparse.ArgumentParser()
    parser.add_argument("--skip-accounts", "-s", required=False,
                            dest="skip_accounts", help="AWS Account alias", metavar="<skip_accounts>")
    parser.parse_args()
    options = parser.parse_args()
    skip_accounts = []

    if options.skip_accounts:
       skip_accounts += options.skip_accounts.split(',')

    for account in accounts:
        if account['Alias'] not in skip_accounts:
            session = get_session(account['Id'])

            print(' - Account: {}'.format(account['Alias']))
            for region in regions:
                client = session.client('ec2', region_name=region)
                default_vpc = DefaultVpc(client)
                print('\t- Region: {}'.format(region), end='')
                if default_vpc.id:
                    default_vpc.delete()
                    print(' (DELETED)')
                else:
                  print(' (NOCHANGE)')
        else:
            print(' - Account: {} (SKIPPED)'.format(account['Alias']))


if __name__ == '__main__':
    main()
