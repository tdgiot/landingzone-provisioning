#!/usr/bin/env python
from __future__ import print_function

import os
import yaml
import boto3

PASSWORD_POLICY = 'data/password_policy.yaml'
STACK_EXEC_ROLE_NAME = 'crave-cloudformation-stack-execution'

def get_client(account_id, region_name, client):
    response = boto3.client('sts').assume_role(
        RoleArn='arn:aws:iam::{}:role/{}'.format(account_id, STACK_EXEC_ROLE_NAME),
        RoleSessionName='crave-landing-zone-provisioning'
    )
    credentials = response['Credentials']

    return boto3.client(
        client,
        region_name=region_name,
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )


def read_password_policy(filename=PASSWORD_POLICY):
    password_policy = yaml.load(open(filename, 'r'))
    return password_policy['PasswordPolicy']


def get_password_policy(client):
    """
    Get IAM password policy
    """
    try:
        return client.get_account_password_policy()['PasswordPolicy']
    except client.exceptions.NoSuchEntityException:
        return {}


def update_password_policy(client, policy):
    client.update_account_password_policy(
        MinimumPasswordLength=policy['MinimumPasswordLength'],
        RequireSymbols=policy['RequireSymbols'],
        RequireNumbers=policy['RequireNumbers'],
        RequireUppercaseCharacters=policy['RequireUppercaseCharacters'],
        RequireLowercaseCharacters=policy['RequireLowercaseCharacters'],
        AllowUsersToChangePassword=policy['AllowUsersToChangePassword'],
        MaxPasswordAge=policy['MaxPasswordAge'],
        PasswordReusePrevention=policy['PasswordReusePrevention'],
        HardExpiry=policy['HardExpiry']
    )


def manage_password_policy():
    print('Setting Password Policy:')
    content = open('./data/accounts.yaml').read()
    body = yaml.load(content)
    accounts = body.get('Accounts', {})
    for account in accounts:
        print('\tAccount: {}'.format(account['Alias']), end='')
        client = get_client(account['Id'], 'ap-southeast-1', 'iam')
        desired_password_policy = read_password_policy()
        current_password_policy = get_password_policy(client)

        if not current_password_policy == desired_password_policy:
            update_password_policy(client, desired_password_policy)
            print(' (UPDATED)')
        else:
            print(' (NOCHANGE)')


if __name__ == '__main__':
    manage_password_policy()
