#!/usr/bin/env python
from __future__ import print_function

import os
import boto3
import yaml
import argparse
from botocore.exceptions import ClientError

STACK_EXEC_ROLE_NAME = 'crave-cloudformation-stack-execution'

def get_session(account_id):
    response = boto3.client('sts').assume_role(
        RoleArn='arn:aws:iam::{}:role/{}'.format(account_id, STACK_EXEC_ROLE_NAME),
        RoleSessionName='crave-landing-zone-provisioning'
    )
    credentials = response['Credentials']

    return boto3.Session(
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )

def get_resource(account_id, region_name, resource):
    response = boto3.client('sts').assume_role(
        RoleArn='arn:aws:iam::{}:role/{}'.format(account_id, STACK_EXEC_ROLE_NAME),
        RoleSessionName='crave-landing-zone-provisioning'
    )
    credentials = response['Credentials']

    return boto3.resource(
        resource,
        region_name=region_name,
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )


def main():
    print('Deleting default security group rules:')

    content = open('./data/accounts.yaml').read()
    body = yaml.load(content)
    accounts = body.get('Accounts', {})

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--skip-accounts", "-s",
        required=False,
        dest="skip_accounts",
        help="AWS Account alias",
        metavar="<skip_accounts>"
    )
    parser.add_argument(
        "--regions", "-r",
        required=True,
        dest="regions",
        help="AWS Regions",
        metavar="<regions>"
    )
    parser.parse_args()
    options = parser.parse_args()

    regions = options.regions.split(',')
    skip_accounts = []
    if options.skip_accounts:
        skip_accounts += options.skip_accounts.split(',')

    for account in accounts:
        account_id = account['Id']
        account_alias = account['Alias']
        if account_alias not in skip_accounts:
            print(' - Account: {}'.format(account_alias))
            session = get_session(account['Id'])
            for region in regions:
                client = session.client('ec2', region_name=region)
                security_groups = client.describe_security_groups()['SecurityGroups']
                for security_group in security_groups:
                    group_name = security_group['GroupName']
                    group_id = security_group['GroupId']
                    ingress = security_group.get('IpPermissions', [])
                    egress = security_group.get('IpPermissionsEgress', [])
                    if group_name == 'default':
                        if len(ingress) > 0 or len(egress) > 0:
                            print('\t- Region: {}'.format(region))
                            ec2 = get_resource(account_id, region, 'ec2')
                            sg = ec2.SecurityGroup(group_id)
                            sg.load()
                            if len(ingress) > 0:
                                print('\t\t- Revoking Ingress rules...', end='')
                                sg.revoke_ingress(IpPermissions=sg.ip_permissions)
                                print(' (DONE)')
                            elif len(egress) > 0:
                                print('\t\t- Revoking Egress rules...', end='')
                                sg.revoke_egress(IpPermissions=sg.ip_permissions_egress)
                                print(' (DONE)')
        else:
            print(' - Account: {} (SKIPPED)'.format(account['Alias']))


if __name__ == '__main__':
    main()
