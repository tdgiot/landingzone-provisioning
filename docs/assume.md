# Switch Role in AWS Console

## Prerequisits

For assuming to another account [ReadMore](https://docs.aws.amazon.com/console/iam/roles-switchrole-console-howtoswitch), you need have an account inside the CRAVE-TGD-IAM account, and this account needs to have [MFA](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html#enable-virt-mfa-for-iam-user) enabled.

## Asume Role

At the moment, three roles are configured inside the CRAVE-IAM account. An administrator role, developer role and a read-only role.
For the administrator role, access to all accounts is granted, and Oblcc and specified Crave users have this role.
The developer role is used for access to the accounts without access to certain iam parts.

To assume to another account, you need to switch via the switch role function in the right top of the screen, see screenshot below
![assume](img/assume1.png "Assume to other account")

![assume 2](img/assume2.png "Assume to other account 2")

| Links Administrator                                                                                                              |
| -------------------------------------------------------------------------------------------------------------------------------- |
| [Administrator-IAM](https://signin.aws.amazon.com/switchrole?account=crave-tdg-iam&roleName=crave-administrator)                 |
| [Administrator-Audit](https://signin.aws.amazon.com/switchrole?account=crave-tdg-audit&roleName=crave-administrator)             |
| [Administrator-Backup](https://signin.aws.amazon.com/switchrole?account=crave-tdg-backup&roleName=crave-administrator)           |
| [Administrator-Build](https://signin.aws.amazon.com/switchrole?account=crave-tdg-build&roleName=crave-administrator)             |
| [Administrator-Logging](https://signin.aws.amazon.com/switchrole?account=crave-tdg-logging&roleName=crave-administrator)         |
| [Administrator-SharedServices](https://signin.aws.amazon.com/switchrole?account=crave-tdg-shs&roleName=crave-administrator)      |
| CraveCloud:                                                                                                                      |
| [Administrator-Development](https://signin.aws.amazon.com/switchrole?account=crave-tdg-development&roleName=crave-administrator) |
| [Administrator-Production](https://signin.aws.amazon.com/switchrole?account=crave-tdg-production&roleName=crave-administrator)   |
