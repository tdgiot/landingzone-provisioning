# AWS Account Bootstrap

## Updating templates

1. Create necessary AWS accounts
2. Enable MFA on all root accounts
3. Update `/data/accounts.yaml` with AWS account information
4. For each `settings.yaml` file in the `templates` folder update the `AccountAlias` and `region` to reflect your newly created accounts and the region you want to deploy to
5. Modify the `bootstrap` yaml files to reflect the new AWS accounts
6. Search for `BucketName` in the templates files and make sure that each bucket name is (globally) unique
7. Update the `0002-environment-parameters` templates

## Bootstrapping the accounts

1. Create a regular user account in master AWS account with assume admin role permissions into sub-accounts
2. Setup AWS credentials on local machine (ClientId & Secret Key)
3. Test if it works by executing `aws sts get-caller-identity` in a terminal window
4. Assume the `OrganizationAccountAccessRole` role from the master account into your sub-accounts using the command below. Change the account ids to reflect your own accounts.

```
aws sts assume-role --role-arn "arn:aws:iam::123456789123:role/OrganizationAccountAccessRole" --role-session-name AWSCLI-Session
```

The AWS CLI command outputs several pieces of information. Inside the credentials block you need the AccessKeyId, SecretAccessKey, and SessionToken.
This example uses the environment variables RoleAccessKeyID, RoleSecretKey, and RoleSessionToken.

Note the timestamp of the expiration field, which is in the UTC time zone and indicates when the temporary credentials of the IAM role will expire.
If the temporary credentials are expired, you must invoke the sts:AssumeRole API call again.

5. Create three environment variables to assume the IAM role. These environment variables are filled out with this output:

```
export AWS_ACCESS_KEY_ID=RoleAccessKeyID
export AWS_SECRET_ACCESS_KEY=RoleSecretKey
export AWS_SESSION_TOKEN=RoleSessionToken
```

6. Verify this works by running: `aws sts get-caller-identity`. The command should return and ANR which will look like:

```
arn:aws:sts::123456789123:assumed-role/OrganizationAccountAccessRole/AWSCLI-Session
```

7. Deploy the account bootstrapping cloudformation template

```
aws cloudformation deploy --template-file 0000-account-bootstrap.cfn.yaml --stack-name crave-account-bootstrap --capabilities CAPABILITY_NAMED_IAM
```

8. Before being able to assume into the next account, first unset the environment variables

```
unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN
aws sts get-caller-identity
```

9. If any accounts left to bootstrap goto step 4, otherwise continue to the next step

10. Assume role into the Build account (use assume-role command from above) and deploy the pipeline cloudformation template

```
aws cloudformation deploy --template-file 0000-landing-zone-provisioning-pipeline.cfn.yaml --stack-name crave-landingzone-provisioning --capabilities CAPABILITY_NAMED_IAM
```

11. The pipeline will not be automatically be able to run. You have to give the pipeline permissions to access BitBucket. For this log in to the build account using the AWS console and goto 'CodePipeline'

12. Open the `crave-tdg-landing-zone-provisioning` pipeline and edit the Source stage and then edit the single step within this stage

13. The dialog that opens will show a manual step is required to connect to BitBucket. Click on the button to connect and follow the instructions. It's adviced to have a service account within BitBucket which you use to create this connection. When using a regular user it's possible that when this user gets disabled the pipeline stops working

14. Once the BitBucket connection has been made, run the pipeline to test and start the initial deployment

## Create build account for AWS CDK

If you will be using the AWS CDK to deploy infrastructure and code using other CI/CD platforms outside of AWS (such as Azure Devops, GitHub, Jenkins, etc...), a separate user with programatical access key is needed.

1. Log in to the build account
2. Create a new IAM user with programatical access
3. Assign the user the following asuume policy

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Resource": [
                "arn:aws:iam::*:role/crave-cloudformation-stack-execution"
            ],
            "Effect": "Allow"
        }
    ]
}
```

This will allow the user to assume the `crave-cloudformaiton-stack-execution` role to any other account

4. Store the access key and secret in a safe place

&nbsp;

---

&nbsp;

Welcome to the Crave documentation. This intranet site will give you guidance to the Crave infrastructure on AWS. This documentation is build with [MkDocs](https://www.mkdocs.org)
