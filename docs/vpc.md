# VPC CIDR

CIDR block 10.255.0.0/16 is set when an account shouldn't use a VPC at all.
For crave-build and crave-sharedservices a CIDR block is reserved and added to the bootstrap CFN template.

| CIDR block    | Region         | AWS alias             | CreateVPC | Multi NATGW |
| ------------- | -------------- | --------------------- | --------- | ----------- |
| 10.255.0.0/16 | ap-southeast-1 | crave-tdg-audit       | false     | false       |
| 10.255.0.0/16 | ap-southeast-1 | crave-tdg-backup      | false     | false       |
| 10.0.0.0/16   | ap-southeast-1 | crave-tdg-build       | false     | false       |
| 10.255.0.0/16 | ap-southeast-1 | crave-tdg-iam         | false     | false       |
| 10.255.0.0/16 | ap-southeast-1 | crave-tdg-logging     | false     | false       |
| 10.1.0.0/16   | ap-southeast-1 | crave-tdg-shs         | false     | false       |
| 10.2.0.0/16   | ap-southeast-1 | crave-tdg-production  | true      | true        |
| 10.5.0.0/16   | ap-southeast-1 | crave-tdg-development | true      | false       |
