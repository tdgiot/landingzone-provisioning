# Config Local environment

The Crave landing zone Bitbucket repository contains scripts and CloudFormation templates which provision the complete Crave AWS environment via CodeBuild and CodePipeline. As we use infrastructure as code, changes needs to be done in the CloudFormation templates instead of via the AWS Console directly. When the new code is checked in in the Bitbucket `landingzone-provisioning` repository, a build process will automatically provision new resources.

To maintain, update and managing the Crave platform, check out the repository and configure your local development environment, as described below.

## Prerequisites

- Linux
- Install [Homebrew](https://brew.sh/)
- Install python 2 and 3
  - `brew install python@2 python@3`
- Ensure that `/usr/local/bin` is at the beginning of your `PATH` variable:
  - `echo $PATH`
- Check if HomeBrew is well configured.
  - Run `brew doctor` and fix possible problems.
- Confirm that python2 is the default
  - `which python` should point to `/usr/local/bin/python`
  - `which pip` should point to `/usr/local/bin/pip`
  - `python -V` should return version 2.7.10 or higher
  - `pip -V` should return version 9.0.3 or higher
- Optionally upgrade `pip`:
  - `pip install -U pip`
- Install virtualenv and virtualenvwrapper:
  - [virtualenv](http://www.virtualenv.org/) (>= 1.11.4) `pip install virtualenv`
  - [virtualenvwrapper](http://virtualenvwrapper.readthedocs.org/) (>= 4.2) `pip install virtualenvwrapper`

## Environment configuration

- Add the following lines to the `~/.zshrc` or `~/.bashrc` file:

```bash
# VirtualEnv
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/home/linuxbrew/.linuxbrew/bin/python3
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true

if [ -e /home/linuxbrew/.linuxbrew/bin/virtualenvwrapper.sh ]
then
  source /home/linuxbrew/.linuxbrew/bin/virtualenvwrapper.sh
else
 if [ -e /usr/share/virtualenvwrapper/virtualenvwrapper.sh ]
 then
   source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
 else
   echo "Error: virtualenvwrapper.sh not found"
 fi
fi
```

- Create a virtual environment:

`mkvirtualenv crave-tdg`
This creates a virtual environment under the name _crave-tdg_. The environment and all its files & directories are stored in `~/.virtualenvs/crave-tdg`.

- Switch to the virtual environment:

`workon crave-tdg`

- Install the Python requirements using pip:

`pip install pip -U`

- Checkout the crave-provisioning repo en navigate to this repo:

``git clone git@bitbucket.org:tdgiot/landingzone-provisioning.git`

`cd landingzone-provisioning`

- Install requirements with pip:

`pip install -r requirements.txt`

- Install pre-commit hook:

  `pre-commit install`

- Add the folllowing to the file `~/.virtualenvs/crave-tdg/bin/postactivate`:

```bash
PS1="$_OLD_VIRTUAL_PS1"
_OLD_RPROMPT="$RPROMPT"
RPROMPT="%{${fg_bold[white]}%}(env: %{${fg[green]}%}`basename \"$VIRTUAL_ENV\"`%{${fg_bold[white]}%})%{${reset_color}%} $RPROMPT"

setopt SH_WORD_SPLIT

# AWS profile for AWS cli
export AWS_PROFILE=crave-tdg-iam
```

- Add the following to the file `~/.virtualenvs/crave/bin/postdeactivate`:

```bash
#!/bin/zsh
RPROMPT="$_OLD_RPROMPT"

unset AWS_PROFILE
```

## AWS CLI/SDK

- Login to the [crave-iam](https://crave-iam.signin.aws.amazon.com/console) account and create your access keys

- Store the `Access Key ID` and `Secret access key` in a secure place (preferable a password vault)

- Ensure that an `.aws` directory exists in your home directory:

`test -d ~/.aws || mkdir ~/.aws`

- Create the AWS CLI config `~/.aws/config` and replace mfa_serial with your Assigend MFA device ARN in the IAM security_credentials section:

For Administrators:

```bash

[profile crave-tdg-iam]
role_arn = arn:aws:iam::872778226783:role/crave-administrator

[profile crave-tdg-audit]
role_arn = arn:aws:iam::317621046023:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-backup]
role_arn = arn:aws:iam::310060301355:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-build]
role_arn = arn:aws:iam::514368322204:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-logging]
role_arn = arn:aws:iam::120753239793:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-shs]
role_arn = arn:aws:iam::431193470299:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-development]
role_arn = arn:aws:iam::888310962618:role/crave-administrator
source_profile = crave-tdg-iam

[profile crave-tdg-production]
role_arn = arn:aws:iam::957352682348:role/crave-administrator
source_profile = crave-tdg-iam

```

- Add the access and secret key to the AWS CLI credentials file `~/.aws/credentials`:

```bash

[crave-iam]
aws_access_key_id     = THISISAFAKEACCESSKEY
aws_secret_access_key = THISISAFACKESECRETACCESSKEY

```

- Make sure that the file `~/.aws/credentials` has secure permissions:

```bash

chmod 0600 ~/.aws/credentials

```

## Testing the environment

- Open a new terminal
- Test
  - that you can switch to the crave virtualenv: `workon crave`
  - the environment variable `AWS_PROFILE` is set to `crave-iam`: `echo $AWS_PROFILE`
  - that the AWS cli works: `aws --profile crave-build s3 ls`
  - you can deactivate the environment: `deactivate`
  - the environment variable `AWS_PROFILE` is unset: `env|grep AWS_PROFILE`
